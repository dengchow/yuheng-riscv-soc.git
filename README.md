﻿14:28 2023/7/2  

## 〇、前言

> 玉衡星，北斗七星中最亮的星。

## 一、简介

本项目是为了学习 RISC-V 内核架构，自行设计 RISC-V 内核并进行 SoC 集成。

## 二、功能介绍

### 1. 环境介绍

- 内核及外设：`YuHeng 5-state RISC-V Core`  
- 软件编译器：`GNU MCU Eclipse RISC-V Embedded GCC, 64-bit (gcc version 8.2.0)`  
- 仿真环境：`VCS + DVE/Veridi`  

*注意，当宏 `PRINT_STDIO_SIM` 被定义时，使用 `mscratchcswl` 寄存器来重定向 xprintf，需要编译器支持（可使用 T-Head 提供的 `riscv64-elf-x86_64-20210307` 版本的 gcc）。*

### 2. 地址分配

|起始地址   |结束地址   |大小|外设  |支持|
|:-:        |:-:        |:-: |:-:   |:-: |
|0x0000_0000|0x0fff_ffff|32K |ROM   |√   |
|0x1000_0000|0x1fff_ffff|32K |RAM   |√   |
|0x2000_0000|0x2fff_ffff|256M|TIMER1|√   |
|0x3000_0000|0x3fff_ffff|256M|UART1 |√   |
|0x4000_0000|0x4fff_ffff|256M|UART2 |√   |
|0x5000_0000|0x5fff_ffff|256M|UART3 |√   |
|0x6000_0000|0x6fff_ffff|256M|×     |×   |

*实际使用的空间大小详见 link.lds 文件。*

## 三、使用说明

本项目共四个分支：

- master， VCS 平台下仿真  
- iverilog， iverilog 平台下仿真  
- fpga， Xilinx FPGA 平台  
- gowin， Gowin FPGA 平台  

*不同分支之间代码有细微差异，以适配不同平台，不建议交叉使用。*

### 1. 测试用例

```
sim
├── build.mk        // make 命令
├── config.mk       // make 参数配置
├── coremark        // #1
├── iap             //   #2
├── iap_app         //     #3
├── iap_boot        //     #3
├── rtthread-nano   // #1
├── simple          // #1
├── systick         // #1
├── timer           // #1
├── uart_rxd        // #1
├── uart_txd        // #1
└── xprintf         // #1

注意：

  #1, 此类测试用例从 0 地址启动
  #2, iap 模拟，app 程序作为 c-array 随 boot 程序一起编译
  #3, iap 模拟，iap_app 提供 app 示例，iap_boot 提供 boot 示例，独立编译
```

### 2. 修改编译工具路径

VCS 和 DVE/Veridi 已经被添加到环境变量中，无需额外设置

仅需修改测试程序编译工具的路径（文件路径：`sim/config.mk`）

```
EMBTOOLPATH     = /home/crazy/Tools/compiler/xuantie/v8.4.0/riscv64-elf-x86_64-20210307
EMBTOOLPREFIX   = ${EMBTOOLPATH}/bin/riscv64-unknown-elf
```

### 3. 编译测试程序

#### 1. 测试用例 1

```
coremark
rtthread-nano
simple
systick
timer
uart_rxd
uart_txd
xprintf
```

1. 进入对应的测试用例目录下（以 `simple` 为例）

通过修改 Makefile 文件来更改编译选项，宏 `PRINT_STDIO_SIM` 被定义时，使用 csr 寄存器来重定向 xprintf，仿真速度快，否则使用 uart1 重定向 xprintf

```
CFLAGS         += -O0 -ffunction-sections -fdata-sections
#CFLAGS         += -g
CFLAGS         += -DPRINT_STDIO_SIM
```

2. 编译，运行

```
make build
make compile
make sim

以上命令等价于： make run

以下是 make sim 后的仿真结果

ic-design@bash:simple>make sim
[INFO] simulate ...
Chronologic VCS simulator copyright 1991-2014
Contains Synopsys proprietary information.
Compiler version I-2014.03_Full64; Runtime version I-2014.03_Full64;  Jun 22 15:23 2023
VCD+ Writer I-2014.03_Full64 Copyright (c) 1991-2014 by Synopsys Inc.
[    117140] UART: add:  1+2+..+100 =  5050, expect:  5050
[    224720] UART: sub: -1-2-..-100 = -5050, expect: -5050
[    303640] UART: mul:  1*2*..*8   = 40320, expect: 40320
[    358340] UART: div:  495, expect: 495
[    359680] DEBG: SIMULATION END.
$finish called from file "../../tb/core_data_monitor_tb.v", line 51.
$finish at simulation time            359680000
           V C S   S i m u l a t i o n   R e p o r t 
Time: 359680000 ps
CPU Time:      1.580 seconds;       Data structure size:   0.2Mb
Thu Jun 22 15:24:03 2023
[INFO] execute done
```

3. 查看波形

```
make wave
```

#### 2. 测试用例 2

```
iap
```

本测试用例用于模拟 iap，app 程序作为 c-array 随 boot 程序一起编译

进入 `iap` 目录，依次执行以下命令

```
make build_app
make build
make compile
make sim

以上命令等价于： make build_app && make run

以下是 make sim 后的仿真结果

ic-design@bash:iap>make sim
[INFO] simulate ...
Chronologic VCS simulator copyright 1991-2014
Contains Synopsys proprietary information.
Compiler version I-2014.03_Full64; Runtime version I-2014.03_Full64;  Jun 22 15:23 2023
VCD+ Writer I-2014.03_Full64 Copyright (c) 1991-2014 by Synopsys Inc.
[    135180] UART: load iap file...
[    502100] UART: load finished.
[    531100] UART: jump to 00001000
[    551780] UART: app start.
[    571560] UART: hello, yu.heng!
[    572900] DEBG: SIMULATION END.
$finish called from file "../../tb/core_data_monitor_tb.v", line 51.
$finish at simulation time            572900000
           V C S   S i m u l a t i o n   R e p o r t 
Time: 572900000 ps
CPU Time:      1.940 seconds;       Data structure size:   0.2Mb
Thu Jun 22 15:23:10 2023
[INFO] execute done
```

*需要注意，请参考 lds 文件确定程序文件大小，默认 boot-16KB/app-4KB。*

#### 3. 测试用例 3

```
iap_app
iap_boot
```

本测试用例用于模拟 iap，iap_app 提供 app 示例，iap_boot 提供 boot 示例，独立编译

1. 进入 `iap_app` 目录，依次执行以下命令

```
make build_app
```

2. 进入 `iap_boot` 目录，依次执行以下命令

```
make build_app
make build
make compile
make sim

以上命令等价于： make build_app && make run

以下是 make sim 后的仿真结果（耗时较长）

ic-design@bash:iap_boot>make sim
[INFO] simulate ...
Chronologic VCS simulator copyright 1991-2014
Contains Synopsys proprietary information.
Compiler version I-2014.03_Full64; Runtime version I-2014.03_Full64;  Jun 22 15:30 2023
VCD+ Writer I-2014.03_Full64 Copyright (c) 1991-2014 by Synopsys Inc.
[     26760] UART: recv bin file...
[ 572420680] UART: recv bin file finished.
[ 572446380] UART: hello, yu.heng!
[ 572447720] DEBG: SIMULATION END.
$finish called from file "../../tb/core_data_monitor_tb.v", line 51.
$finish at simulation time         572447720000
           V C S   S i m u l a t i o n   R e p o r t 
Time: 572447720000 ps
CPU Time:    447.050 seconds;       Data structure size:   0.2Mb
Thu Jun 22 15:38:32 2023
[INFO] execute done
```

## 四、致谢

在玉衡（YuHeng）的设计过程中，参考和借鉴了许多优秀的开源项目。

## 五、修改日志

- v3.1_vcs  
  - 测试 sim 目录下全部测试用例（14:28 2023/7/2）  

- v3.0  
  - 代码重构（13:08 2023/6/14）  

- v2.0  
  - 统一几个工程的文件结构（11:45 2022/4/5）  

- v1.0  
  - 创建项目并集成测试（18:05 2022/4/4）  

## 六、维护

如果有任何疑问或者建议，欢迎在下方评论，或者通过邮件联系（E-mail：ytesliang@163.com），我会尽可能在 24 小时内进行回复。

ATONEMAN  
2022.07.02  
