#/*
# * Copyright {c} 2020-2021, SERI Development Team
# *
# * SPDX-License-Identifier: Apache-2.0
# *
# * Change Logs:
# * Date         Author          Notes
# * 2022-04-04   Lyons           first version
# */

COLORS = "\033[32m"
COLORE = "\033[0m"

.PHONY: help
help:
	@echo "help     - help menu             "
	@echo "build    - compile c/asm file    "
	@echo "compile  - compile rtl file      "
	@echo "sim      - simulate              "
	@echo "run      - compile and simulate  "
	@echo "wave     - open wave             "

.PHONY: build
build:
	@echo -e ${COLORS}[INFO] compile c/asm file ...${COLORE}
	${Q}${CC} ${INCLUDES} ${INCFILES} ${CFLAGS} ${LDFLAGS} ${LDLIBS} ${ASMFILES} ${CFILES} -o ${TARGET}.elf
	@echo -e ${COLORS}[INFO] create dump file ...${COLORE}
	${Q}${OBJDUMP} -D -S ${TARGET}.elf > ${TARGET}.dump
	@echo -e ${COLORS}[INFO] create image file ...${COLORE}
	${Q}${OBJCOPY} -S -O binary  ${TARGET}.elf ${TARGET}.bin
	${Q}${OBJCOPY} -S -O verilog ${TARGET}.elf image.pat
	@echo -e ${COLORS}[INFO] execute done${COLORE}

.PHONY: compile
compile:
	@echo -e ${COLORS}[INFO] compile design file ...${COLORE}
	${Q}${VCS} -Mupdate -sverilog +v2k -debug_pp -timescale="1ns/100ps" ${ALLDEFINE} \
		-f ${PROJPATH}/scripts/design.f ${TBFILES} \
		-l compile.log
	@echo -e ${COLORS}[INFO] execute done${COLORE}

.PHONY: sim
sim:
	@echo -e ${COLORS}[INFO] simulate ...${COLORE}
	${Q}${SIM} -l sim.log
	@echo -e ${COLORS}[INFO] execute done${COLORE}

.PHONY: run
run: build compile sim

.PHONY: wave
wave:
	${Q}${WAV} -full64 -vpd wave.vpd &

.PHONY: clean
clean:
	@echo -e ${COLORS}[INFO] clean project ...${COLORE}
	@ls | grep -vE "Makefile|*\.lds$$|*\.c$$|*\.h$$" | xargs -i rm -rf {}
	@echo -e ${COLORS}[INFO] execute done${COLORE}
