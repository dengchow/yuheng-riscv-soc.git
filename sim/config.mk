#/*
# * Copyright {c} 2020-2021, SERI Development Team
# *
# * SPDX-License-Identifier: Apache-2.0
# *
# * Change Logs:
# * Date         Author          Notes
# * 2022-04-04   Lyons           first version
# */

EMBTOOLPATH     = /home/crazy/Tools/compiler/xuantie/v8.4.0/riscv64-elf-x86_64-20210307

EMBTOOLPREFIX   = ${EMBTOOLPATH}/bin/riscv64-unknown-elf

CC              = ${EMBTOOLPREFIX}-gcc
OBJDUMP         = ${EMBTOOLPREFIX}-objdump
OBJCOPY         = ${EMBTOOLPREFIX}-objcopy

VCS             = vcs
SIM             = ./simv
WAV             = dve

RM              = rm -f
CP              = cp
MV              = mv

# for vcs tools
ALLDEFINE       = +define+DUMP_VPD
ALLDEFINE      += +define+TESTBENCH_VCS

TBFILES         = ${PROJPATH}/tb/core_data_monitor_tb.v \
                  ${PROJPATH}/tb/core_uart_monitor_tb.v \
                  ${PROJPATH}/tb/core_tb.v

# for c/asm tools
INCLUDES        = 

INCFILES        = -I${PROJPATH}/libs \
                  -I${PROJPATH}/libs/_sdk \
                  -I${PROJPATH}/libs/_sdk/systick \
                  -I${PROJPATH}/libs/_sdk/timer \
                  -I${PROJPATH}/libs/_sdk/uart \
                  -I${PROJPATH}/libs/_utilities

CFLAGS          = -march=rv32im -mabi=ilp32 -mcmodel=medlow

LDFLAGS         = -Wl,-Map,${TARGET}.map,-warn-common \
                  -Wl,--gc-sections \
                  -Wl,--no-relax \
                  -nostartfiles

LDLIBS          = -lm -lc -lgcc

ASMFILES        = ${PROJPATH}/libs/_startup/start.S \
                  ${PROJPATH}/libs/_startup/trap.S

CFILES          = ${PROJPATH}/libs/_sdk/systick/*.c \
                  ${PROJPATH}/libs/_sdk/timer/*.c \
                  ${PROJPATH}/libs/_sdk/uart/*.c \
                  ${PROJPATH}/libs/_utilities/*.c
