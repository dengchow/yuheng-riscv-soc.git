#/*
# * Copyright {c} 2020-2021, SERI Development Team
# *
# * SPDX-License-Identifier: Apache-2.0
# *
# * Change Logs:
# * Date         Author          Notes
# * 2022-03-27   Lyons           first version
# */

TARGET          = riscv

Q               = @

PROJPATH        = ../..
WORKPATH        = .

include ../config.mk

INCLUDES       += -I.

INCFILES       += 

CFLAGS         += -O0 -ffunction-sections -fdata-sections
#CFLAGS         += -g
CFLAGS         += -DPRINT_STDIO_SIM

LDFLAGS        += -T${WORKPATH}/link_boot.lds

LDLIBS         += 

ASMFILES       += 

CFILES         += ${WORKPATH}/iap.c \
                  ${WORKPATH}/main_boot.c

include ../build.mk

LDFLAGS_APP     = -Wl,-Map,${TARGET}.map,-warn-common \
                  -Wl,--gc-sections \
                  -Wl,--no-relax \
                  -nostartfiles
LDFLAGS_APP    += -T${WORKPATH}/link_app.lds

CFILES_APP      = ${PROJPATH}/libs/_sdk/systick/*.c \
                  ${PROJPATH}/libs/_sdk/timer/*.c \
                  ${PROJPATH}/libs/_sdk/uart/*.c \
                  ${PROJPATH}/libs/_utilities/*.c
CFILES_APP     += ${WORKPATH}/main_app.c

.PHONY: build_app
build_app:
	@echo -e ${COLORS}[INFO] compile c/asm file ...${COLORE}
	${Q}${CC} ${INCLUDES} ${INCFILES} ${CFLAGS} ${LDFLAGS_APP} ${LDLIBS} ${ASMFILES} ${CFILES_APP} -o ${TARGET}.elf
	@echo -e ${COLORS}[INFO] create dump file ...${COLORE}
	${Q}${OBJDUMP} -D -S ${TARGET}.elf > ${TARGET}.dump
	@echo -e ${COLORS}[INFO] create image file ...${COLORE}
	${Q}${OBJCOPY} -S -O binary  ${TARGET}.elf ${TARGET}_app.bin
	${Q}../../scripts/bin2array ${TARGET}_app.bin > iap.c
	@echo -e ${COLORS}[INFO] execute done${COLORE}
