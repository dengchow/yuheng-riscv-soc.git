/*
 * Copyright (c) 2020-2021, SERI Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-29     Lyons        first version
 */

#include "__def.h"
#include "xprintf.h"

#define IAP_ADDR 0x00001000 // from 4KB

typedef void (*func)(void);

int main()
{
    func _jump = (func)IAP_ADDR;

    xprintf("load iap file...\n");

    extern uint32_t g_iap_bin_size;
    extern uint32_t g_iap_bin_buf[];

    for (uint32_t i=0; i<g_iap_bin_size/4; i++) {
        *((uint32_t*)IAP_ADDR + i) = g_iap_bin_buf[i];
    }

    xprintf("load finished.\n");

    xprintf("jump to %08x\n", (uint32_t)_jump);
    _jump();

    simulation(0x4);
    return 0;
}
