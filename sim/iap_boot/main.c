/*
 * Copyright (c) 2020-2021, SERI Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-29     Lyons        first version
 */

#include "__def.h"
#include "systick.h"
#include "xprintf.h"

#define IAP_ADDR 0x00001000 // from 4KB

typedef void (*func)(void);

int main()
{
    func _jump;

    xprintf("recv bin file...\n");

    uint32_t offset;
    offset = 0;

    while (1)
    { 
        uint8_t dummy[16*4+2]; // A5 data32*16 5A
        
        for (uint32_t i=0; i<sizeof(dummy); i++) {
            dummy[i] = uart_read_wait(UART1);
        }

        if (0xA6 == dummy[0]) {
            break;
        }
    
        for (uint32_t i=0; i<16; i++) {
            *((uint32_t*)IAP_ADDR + offset + i) = (dummy[1 + 4*i + 0] << 0)
                                                + (dummy[1 + 4*i + 1] << 8)
                                                + (dummy[1 + 4*i + 2] << 16)
                                                + (dummy[1 + 4*i + 3] << 24);
        }
        
        offset += 16;
    }
        
    xprintf("recv bin file finished.\n");

    _jump = (func)IAP_ADDR;
    _jump();

    simulation(0x4);
    return 0;
}
