`timescale 1ns / 1ps
/*
 * Copyright (c) 2020-2021, SERI Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date             Author      Notes
 * 2021-10-29       Lyons       first version
 * 2022-04-04       Lyons       v2.0
 */

`ifdef TESTBENCH_VCS
`include "pa_chip_param.v"
`else
`include "../rtl/pa_chip_param.v"
`endif

module core_tb(
    );


reg                             sys_clk;
reg                             sys_rst_n;

wire                            PAD_A0;
wire                            PAD_A1;

initial begin
`ifdef DUMP_VPD
    $vcdplusfile("wave.vpd");
    $vcdpluson(0, core_tb);
`endif
end

pa_chip_top u_pa_chip_top (
    .clk_i                      (sys_clk),
    .rst_n_i                    (sys_rst_n),

    .rxd                        (PAD_A1),
    .txd                        (PAD_A0)
);

core_data_monitor_tb u_core_data_monitor_tb (
    .clk_i                      (sys_clk),
    .rst_n_i                    (sys_rst_n),

    .data_i                     (core_tb.u_pa_chip_top.u_pa_core_top.u_pa_core_rtu.u_pa_core_csr._mscratchcswl) 
);

core_uart_monitor_tb u_core_uart_monitor_tb (
    .clk_i                      (sys_clk),
    .rst_n_i                    (sys_rst_n),

    .rxd                        (PAD_A0),
    .txd                        ()
);

`ifdef SIM_UART_IAP
core_uart_iap_tb u_core_uart_iap_tb (
    .clk_i                      (sys_clk),
    .rst_n_i                    (sys_rst_n),

    .rxd                        (),
    .txd                        (PAD_A1)
);
`endif

initial begin
    sys_clk = 1;
    sys_rst_n = 0;

`ifdef MEMORY_MODEL_REG
    $readmemh("image.pat", u_pa_chip_top.u_pa_perips_rom._ram);
`endif
`ifdef MEMORY_MODEL_BRAM
    
`endif
end

always begin
    @ (posedge sys_clk) sys_rst_n = 0;
    @ (posedge sys_clk) sys_rst_n = 1;

    while (1) begin
        @ (posedge sys_clk);
    end

    $stop();
end

always #((32'd1_000_000_000/`XTAL_FREQ_HZ)/2) sys_clk = ~sys_clk;

endmodule
